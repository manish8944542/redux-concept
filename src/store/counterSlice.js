import { createSlice } from "@reduxjs/toolkit";

export const counterSlice = createSlice({
    //this name is very important and has to be unique
    name : 'counter',
    initialState : {
        value : 0
    },
    reducers: {
        increment: function(currentState, action) {
            console.log("Increment Called!", action); // action mei wh aaega jo hum component ke dispatch wale increment mei bhej rhe aur console pe as a obj dikhega ye payload ke andar
            // return currentState;
            return { ...currentState, value: currentState.value + 1} // plus 1 wala logic
        },
        decrement: function(currentState, action) {
            console.log("Decrement Called!");
            // return currentState;
            return { ...currentState, value: currentState.value - 1}
        },

    }
})

export const { increment,decrement } = counterSlice.actions; // upar reducers ke andar jo keywords hai woh actions dwnote karte aur woh left side destructure hoke baith jaenge
const counterReducer = counterSlice.reducer; // isme pura reducers wala object aajaega
export default counterReducer; // hum export isiliye kar rahe taaki store mei jaake daal sake ye slice ke reducer ko