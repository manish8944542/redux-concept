import { configureStore } from "@reduxjs/toolkit";
import counterReducer from "./counterSlice";

//ye config store mei hum saare slices ke reducers ko daalke rakhenge
export default configureStore({
    reducer: {
        counter : counterReducer
    }
})
