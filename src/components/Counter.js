import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { decrement, increment } from '../store/counterSlice';

export const Counter = () => {
    const value = useSelector(state => state.counter.value); // iske andar store ke saare states aajaege aur ye un dtates mei se counterSlice mei jo name hai counter uska value dedega jo initialValue ke andar hai
    const dispatch = useDispatch();

    const handleIncrement = () => {
        dispatch(increment("Hello World!")) // ye jaega counterslice ke increment ke action mei
    }
    const handleDecrement = () => {
        dispatch(decrement("Hello World!"))
    }

  return (
    <div>
        <button onClick={ handleIncrement } style={ { marginRight: '5px'}}>
            Add
        </button>
        { value }
        <button onClick={ handleDecrement } style={ { marginLeft: '5px'}}>
            Sub
        </button>
    </div>
  )
}
