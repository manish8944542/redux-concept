import React from 'react';
import { Provider } from 'react-redux';
import store from './store/store';
import { Counter } from './components/Counter';

export default function App() {
  return (
    //bohot saare providers se acha ek hi provider jisko saare reducers mile store se isiliye store liya Provider ne
    <Provider store={store}>
      <Counter /> 
      {/* ab counter component ko saare store ka access mil jaega */}
    </Provider>
  )
}
